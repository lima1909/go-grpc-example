package main

import (
	"context"
	"fmt"

	"gitlab.com/lima1909/go-grpc-example/echo"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial(":9090", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	client := echo.NewEchoServiceClient(conn)
	echo, err := client.Echo(context.Background(), &echo.EchoRequest{Echo: "Yay ...."})
	if err != nil {
		panic(err)
	}

	fmt.Println(echo.GetEcho())
}
