package main

import (
	"context"
	"fmt"
	"net"

	"gitlab.com/lima1909/go-grpc-example/echo"
	"google.golang.org/grpc"
)

type echoServer struct{}

func (e echoServer) Echo(ctx context.Context, req *echo.EchoRequest) (*echo.EchoResponse, error) {
	return &echo.EchoResponse{Echo: "ECHO: " + req.GetEcho()}, nil
}

func main() {
	srv := grpc.NewServer()
	var esrv echoServer
	echo.RegisterEchoServiceServer(srv, esrv)
	l, err := net.Listen("tcp", ":9090")
	if err != nil {
		panic(err)
	}

	fmt.Println("start server on port :9090")
	panic(srv.Serve(l))
}
